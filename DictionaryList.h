#pragma once
#include <iostream>
#include "List.h"

class DictionaryList {

public:
	DictionaryList();
	~DictionaryList();

	bool addWord(int key);
	bool searchWord(int key);
	bool deleteWord(int key);
	bool isEmpty();

	DictionaryList& merge(DictionaryList& second);
	DictionaryList& deleteDictionary(const DictionaryList& second);

	friend DictionaryList& getIntersection(const DictionaryList& first, const DictionaryList& second);

	friend std::ostream& operator<<(std::ostream& out, const DictionaryList& first);

private:
	List<int> dict;
};
