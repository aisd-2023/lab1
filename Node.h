#pragma once

template <typename T>
class Node {

public:
	Node();
	Node(const Node<T>& other);
	Node(Node<T>&& other) noexcept;
	Node(const T& value);
	~Node();
	Node<T>& operator=(const Node<T>& other);
	Node<T>& operator=(Node<T>&& other) noexcept;
	bool operator==(const Node<T>& other);
	bool operator==(const T& value);
	bool operator<(const Node<T>& other);
	bool operator<(const T& value);
	Node<T>* getNext() const;
	void setNext(Node<T>* pNode);
	
	friend std::ostream& operator<<(std::ostream& out, const Node<T>& node);

	T value;

private:
	Node<T>* next;
};

// -----------------------------------------------------------------

template<typename T>
Node<T>::Node()
	: value(T())
	, next(nullptr)
{

}

template<typename T>
Node<T>::Node(const Node<T>& other)
	: value(other.value)
	, next(other.next)
{

}

template<typename T>
Node<T>::Node(Node<T>&& other) noexcept
	: value(other.value)
	, next(other.next)
{
	other.value = T();
	other.next = nullptr;
}

template<typename T>
Node<T>::Node(const T& otherValue)
	: value(otherValue)
	, next(nullptr)
{

}

template<typename T>
Node<T>::~Node()
{
	value = T();
	next = nullptr;
}

template<typename T>
Node<T>& Node<T>::operator=(const Node<T>& other)
{
	value = other.value;
	next = other.next;
}

template<typename T>
Node<T>& Node<T>::operator=(Node<T>&& other) noexcept
{
	value = other.value;
	other.value = T();
	next = other.next;
	other.next = nullptr;
}

template<typename T>
bool Node<T>::operator==(const Node<T>& other)
{
	return (value == other.value);
}

template<typename T>
bool Node<T>::operator==(const T& otherValue)
{
	return (value == otherValue);
}

template<typename T>
bool Node<T>::operator<(const Node<T>& other)
{
	return (value < other.value);
}

template<typename T>
bool Node<T>::operator<(const T& otherValue)
{
	return (value < otherValue);
}

template<typename T>
Node<T>* Node<T>::getNext() const
{
	return next;
}

template<typename T>
void Node<T>::setNext(Node<T>* pNode)
{
	next = pNode;
}

