#include "DictionaryList.h"

DictionaryList::DictionaryList()
	: dict()
{

}

DictionaryList::~DictionaryList()
{

}

bool DictionaryList::addWord(int key)
{
	return dict.insert(key);
}

bool DictionaryList::searchWord(int key)
{
	Node<int>* pPrefirstNode = nullptr;
	Node<int>* pFirstNode = nullptr;
	if (dict.find(key, pFirstNode, pPrefirstNode)) {
		return (pFirstNode != nullptr && pFirstNode->value == key);
	}

	return false;
}

bool DictionaryList::deleteWord(int key)
{
	return dict.deleteNode(key);
}

bool DictionaryList::isEmpty()
{
	return dict.isEmpty();
}

DictionaryList& DictionaryList::merge(DictionaryList& second)
{
	Node<int>* pSecondNode = second.dict.getHead();
	while (pSecondNode != nullptr) {
		Node<int>* pFirstNode = nullptr;
		Node<int>* pPrefirstNode = nullptr;
		if (dict.find(pSecondNode->value, pFirstNode, pPrefirstNode)) {
			if (pFirstNode != nullptr) {
				if (pFirstNode->value == pSecondNode->value) {
					if (!second.dict.deleteFirstNode()) break;
				}
				else if (!moveNode(pPrefirstNode, second.dict.getRoot())) break;
			} 
			else if (pPrefirstNode != nullptr) {
				if (!moveNode(pPrefirstNode, second.dict.getRoot())) break;
			}
			else break;
		} 
		else break;
		pSecondNode = second.dict.getHead();
	}

	return *this;
}

DictionaryList& DictionaryList::deleteDictionary(const DictionaryList& second)
{
	Node<int>* pSecondNode = second.dict.getHead();
	while (pSecondNode != nullptr) {
		Node<int>* pFirstNode = nullptr;
		Node<int>* pPrefirstNode = nullptr;
		if (dict.find(pSecondNode->value, pFirstNode, pPrefirstNode)) {
			if (pFirstNode != nullptr) {
				if (pFirstNode->value == pSecondNode->value) {
					if (pPrefirstNode != nullptr) {
						if (!dict.deleteNextNode(pPrefirstNode)) break;
					}
				}
			}
		}
		pSecondNode = pSecondNode->getNext();
	}

	return *this;
}

DictionaryList& getIntersection(const DictionaryList& first, const DictionaryList& second)
{
	DictionaryList* result = new DictionaryList;

	Node<int>* pSecondNode = second.dict.getHead();
	while (pSecondNode != nullptr) {
		Node<int>* pFirstNode = nullptr;
		Node<int>* pPrefirstNode = nullptr;
		if (first.dict.find(pSecondNode->value, pFirstNode, pPrefirstNode)) {
			if (pFirstNode != nullptr) {
				if (pFirstNode->value == pSecondNode->value) {
					result->addWord(pSecondNode->value);
				}
			}
		}
		pSecondNode = pSecondNode->getNext();
	}

	return *result;
}

std::ostream& operator<<(std::ostream& out, const Node<int>& node)
{
	return (out << node.value);
}

std::ostream& operator<<(std::ostream& out, const List<int>& first)
{
	Node<int>* pNode = first.getHead();
	while (pNode != nullptr) {
		out << *pNode << "->";
		pNode = pNode->getNext();
	}
	return (out << "null");
}

std::ostream& operator<<(std::ostream& out, const DictionaryList& first)
{
	return (out << first.dict);
}
