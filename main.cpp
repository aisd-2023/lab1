﻿#include <iostream>
#include <string>
#include "DictionaryList.h"

int main()
{
	try {
		DictionaryList testDictionary;
		std::cout << "Create empty dictionary:" << std::endl;
		std::cout << testDictionary << std::endl;
		std::cout << "The dictionary is empty:" << (testDictionary.isEmpty() ? "True" : "False");
		std::cout << std::endl;

		testDictionary.addWord(5);
		testDictionary.addWord(3);
		testDictionary.addWord(8);
		testDictionary.addWord(-2);
		testDictionary.addWord(7);
		testDictionary.addWord(139);
		testDictionary.addWord(80);
		std::cout << "Create new dictionary:" << std::endl;
		std::cout << testDictionary << std::endl;
		std::cout << std::endl;

		int elem = 80;
		std::string isFound = (testDictionary.searchWord(elem) ? "True" : "False");
		std::cout << "Element " << elem << " is found in dictionary: " << isFound << std::endl;
		elem = 10;
		isFound = (testDictionary.searchWord(elem) != 0) ? "True" : "False";
		std::cout << "Element " << elem << " is found in dictionary: " << isFound << std::endl;
		std::cout << std::endl;

		if (testDictionary.deleteWord(3)) std::cout << "Delete word 3" << std::endl;
		if (testDictionary.addWord(15)) std::cout << "Add word 15" << std::endl;
		std::cout << "Dictionary after changes:" << std::endl;
		std::cout << testDictionary << std::endl;
		std::cout << std::endl;
		std::cout << "==============================================================" << std::endl;

		DictionaryList testDictionary1;
		testDictionary1.addWord(10);
		testDictionary1.addWord(-2);
		testDictionary1.addWord(0);
		testDictionary1.addWord(139);
		testDictionary1.addWord(-7);
		testDictionary1.addWord(25);
		testDictionary1.addWord(9);
		std::cout << "New dictionary 1:" << std::endl;
		std::cout << testDictionary1 << std::endl;
		std::cout << std::endl;

		DictionaryList testDictionary2;
		testDictionary2.addWord(10);
		testDictionary2.addWord(-50);
		testDictionary2.addWord(11);
		testDictionary2.addWord(139);
		testDictionary2.addWord(-7);
		std::cout << "New dictionary 2:" << std::endl;
		std::cout << testDictionary2 << std::endl;
		std::cout << std::endl;

		testDictionary1.merge(testDictionary2);
		std::cout << "Dictionary 1 after merging: " << std::endl;
		std::cout << testDictionary1 << std::endl;
		std::cout << "Dictionary 2 after merging: " << std::endl;
		std::cout << testDictionary2 << std::endl;
		std::cout << std::endl;

		testDictionary2.addWord(10);
		testDictionary2.addWord(-50);
		testDictionary2.addWord(11);
		testDictionary2.addWord(139);
		testDictionary2.addWord(-7);

		std::cout << "Update dictionary 2:" << std::endl;
		std::cout << testDictionary2 << std::endl;
		std::cout << std::endl;

		testDictionary1.deleteDictionary(testDictionary2);
		std::cout << "Dictionary 1 after deleting: " << std::endl;
		std::cout << testDictionary1 << std::endl;
		std::cout << "Dictionary 2 after deleting: " << std::endl;
		std::cout << testDictionary2 << std::endl;
		std::cout << std::endl;

		testDictionary1.addWord(10);
		testDictionary1.addWord(-2);
		testDictionary1.addWord(0);
		testDictionary1.addWord(139);
		testDictionary1.addWord(-7);
		testDictionary1.addWord(25);
		testDictionary1.addWord(9);
		std::cout << "Update dictionary 1:" << std::endl;
		std::cout << testDictionary1 << std::endl;
		std::cout << std::endl;

		std::cout << "Intersection dictionary: " << std::endl;
		std::cout << getIntersection(testDictionary1, testDictionary2) << std::endl;
	} 
	catch (std::exception const& e) {
		std::cerr << "Error: " << e.what() << "\n";
		exit(1);
	}
	
	return 0;
}