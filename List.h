#pragma once
#include <limits>
#include "Node.h"

template <typename T>
class List {

	friend class Node<T>;

public:
	List();
	List(const List& other);
	List(List&& other) noexcept;
	~List();
	List& operator=(const List& other);
	List& operator=(List&& other) noexcept;
	bool find(const T& findValue, Node<T>*& foundNode, Node<T>*& prefoundNode) const;
	bool insert(const T& value);
	bool insert(Node<T>&& node);
	bool deleteFirstNode();
	bool deleteNextNode(Node<T>* prevNode);
	bool deleteNode(const T& value);
	Node<T>* getRoot() const;
	Node<T>* getHead() const;
	bool isEmpty() const;

	friend bool moveNode(Node<T>* afterDestNode, Node<T>* prevSrcNode);
	friend std::ostream& operator<<(std::ostream& out, const List<T>& first);

private:
	void copyList(Node<T>* dstList, Node<T>* srcList);
	void deleteList(Node<T>* listRoot);
	Node<T>* root;
};

// -----------------------------------------------------------

template<typename T>
List<T>::List()
	: root(new Node<T>(T(INT_MIN)))
	// : root(nullptr) // For testing exception
{

}

template<typename T>
List<T>::List(const List<T>& other)
	: root(new Node<T>(T(INT_MIN)))
{
	copyList(root, other.root);
}

template<typename T>
List<T>::List(List<T>&& other) noexcept
	: root(new Node<T>(T(INT_MIN)))
{
	root.next = other.root->next;
	other.root.next = nullptr;
}

template<typename T>
List<T>::~List()
{
	deleteList(root);
	if (root != nullptr) delete root;
	root = nullptr;
}

template<typename T>
List<T>& List<T>::operator=(const List<T>& other)
{
	deleteList(root);
	if (root == nullptr) root = new Node<T>;
	copyList(root, other.root);
}

template<typename T>
List<T>& List<T>::operator=(List<T>&& other) noexcept
{
	deleteList(root);
	root = other.root;
	other.root = nullptr;
}

template<typename T>
bool List<T>::find(const T& findValue, Node<T>*& foundNode, Node<T>*& prefoundNode) const
{
	if (root == nullptr) {
		throw std::runtime_error("List is corrupted");
	}

	Node<T>* pNode = root;
	Node<T>* pNextNode = pNode->getNext();
	while (pNextNode != nullptr && pNextNode->value < findValue) {
		pNode = pNextNode;
		pNextNode = pNode->getNext();
	}
	prefoundNode = pNode;
	foundNode = pNextNode;

	return true;
}

template<typename T>
bool List<T>::insert(const T& insertValue)
{
	if (root == nullptr) {
		throw std::runtime_error("List is corrupted");
	}

	Node<T>* pPrevNode = root;
	Node<T>* pNode = pPrevNode->getNext();
	while (pNode != nullptr && pNode->value < insertValue) {
		pPrevNode = pNode;
		pNode = pNode->getNext();
	}
	if (pNode != nullptr && pNode->value == insertValue) return false;
	if (pPrevNode != nullptr) {
		Node<T>* pNewNode = new Node<T>;
		pNewNode->value = insertValue;
		pNewNode->setNext(pNode);
		pPrevNode->setNext(pNewNode);
		return true;
	}

	return false;
}

template<typename T>
bool List<T>::insert(Node<T>&& insertNode)
{
	if (root == nullptr) {
		throw std::runtime_error("List is corrupted");
	}
	if (insertNode == nullptr) return false;

	Node<T>* pPrevNode = root;
	Node<T>* pNode = pPrevNode->next;
	T insertValue = insertNode.value;
	while (pNode != nullptr && pNode->value < insertValue) {
		pPrevNode = pNode;
		pNode = pNode->next;
	}
	if (pNode != nullptr && pNode->value == insertValue) return false;
	if (pPrevNode != nullptr) {
		pPrevNode->next = &insertNode;
		return true;
	}

	return false;
}

template<typename T>
bool List<T>::deleteFirstNode()
{
	Node<T>* pNode = root->getNext();
	if (pNode != nullptr) {
		root->setNext(pNode->getNext());
		delete pNode;
	}
	return true;
}

template<typename T>
bool List<T>::deleteNextNode(Node<T>* prevNode)
{
	if (prevNode == nullptr) return false;

	Node<T>* pNextNode = prevNode->getNext();
	if (pNextNode != nullptr) {
		prevNode->setNext(pNextNode->getNext());
		delete pNextNode;
	}

	return true;
}

template<typename T>
bool List<T>::deleteNode(const T& deleteValue)
{
	if (root == nullptr) {
		throw std::runtime_error("List is corrupted");
	}

	Node<T>* pPrevNode = root;
	Node<T>* pNode = pPrevNode->getNext();
	while (pNode != nullptr && pNode->value < deleteValue) {
		pPrevNode = pNode;
		pNode = pNode->getNext();
	}
	if (pNode != nullptr) {
		if (pNode->value != deleteValue) return false;
		if (pPrevNode != nullptr) {
			pPrevNode->setNext(pNode->getNext());
		}
		delete pNode;
		return true;
	}

	return false;
}

template<typename T>
void List<T>::copyList(Node<T>* dstList, Node<T>* srcList)
{
	if (srcList == nullptr) return;

	if (dstList == nullptr) {
		dstList = new Node<T>;
	}

	Node<T>* pDstNode = dstList;
	Node<T>* pSrcNode = srcList;
	while (pSrcNode->next != nullptr) {
		pDstNode->next = new Node<T>;
		pDstNode = pDstNode->next;
		pSrcNode = pSrcNode->next;
		pDstNode->value = pSrcNode->value;
	}
}

template<typename T>
void List<T>::deleteList(Node<T>* listRoot)
{
	if (listRoot == nullptr) return;

	Node<T>* pNode = listRoot->getNext();
	while (pNode != nullptr) {
		Node<T>* pNextNode = pNode->getNext();
		delete pNode;
		pNode = pNextNode;
	}
}

template<typename T>
Node<T>* List<T>::getHead() const
{
	if (root == nullptr) {
		throw std::runtime_error("List is corrupted");
	}
	
	return root->getNext();
}

template<typename T>
Node<T>* List<T>::getRoot() const
{
	return root;
}

template<typename T>
bool List<T>::isEmpty() const
{
	return getHead() == nullptr;
}

template<typename T>
bool moveNode(Node<T>* afterDestNode, Node<T>* prevSrcNode)
{
	if (afterDestNode == nullptr || prevSrcNode == nullptr) return false;

	Node<T>* pMovedNode = prevSrcNode->getNext();
	if (pMovedNode == nullptr) return false;

	if (afterDestNode->value < pMovedNode->value) {
		Node<T>* pNextAfterDestNode = afterDestNode->getNext();
		if (pNextAfterDestNode != nullptr) {
			if (pMovedNode->value < pNextAfterDestNode->value) {
				prevSrcNode->setNext(pMovedNode->getNext());
				afterDestNode->setNext(pMovedNode);
				pMovedNode->setNext(pNextAfterDestNode);
			}
			else {
				return false;
			}
		}
		else {
			prevSrcNode->setNext(pMovedNode->getNext());
			afterDestNode->setNext(pMovedNode);
			pMovedNode->setNext(nullptr);
		}
	}
	else {
		return false;
	}

	return true;
}
